(function (){
    var expires = parseInt(localStorage.getItem('auth.expires') || 0);
    if (expires < new Date().getTime()) {
        window.location = 'auth/go';
    }
    var token = localStorage.getItem('auth.token');
    function request (url, ctx) {
        var async = ctx.async || true;
        var json = ctx.json || true;
        var data = ctx.json ? JSON.stringify(ctx.data) : ctx.data;
        var method = ctx.data ? 'POST' : 'GET';
        var data_type = ctx.json ? 'application/json' : ctx.data_type || 'text/plain';
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var res = xhr.responseText;
            if (json) {
                res = JSON.parse(res);
            }
            console.log(res);
            ctx.loaded(res);
        }
        xhr.open(method, url, async);
        if (ctx.data) {
            xhr.setRequestHeader("Content-type", data_type);
        }
        xhr.setRequestHeader('Authorization', 'Bearer ' + token);
        xhr.send(data);
    }
    //var folder_id = localStorage.getItem('folder_id');
    var file_id = localStorage.getItem('file_id');
    var appfolder_id = localStorage.getItem('appfolder_id') || 'appfolder';
    if (!file_id) {
        function make_file () {
            var default_data = JSON.stringify({
                list: [],
                lookup: []
            });
            var metadata = {
                title: 'mileage_record.json',
                mimeType: 'application/json',
                parents: [{'id': appfolder_id}]
            };
            var boundary = '-------9000fuzzyelbows-------';
            var delimiter = "\r\n--" + boundary + "\r\n";
            var close_delim = "\r\n--" + boundary + "--";
            var body = 
                delimiter +
                'Content-Type: application/json\r\n\r\n' +
                JSON.stringify(metadata) +
                delimiter +
                'Content-Type: application/json\r\n' +
                '\r\n' +
                localStorage.trips || default_data +
                close_delim;

            request('https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart', {
                data: body,
                data_type: 'multipart/mixed; boundary="' + boundary + '"',
                json: false,
                loaded: function (res) {
                    res = JSON.parse(res);
                    file_id = res.id;
                    localStorage.setItem('file_id', file_id);
                    request('https://www.googleapis.com/drive/v2/files/' + appfolder_id + '/properties', {
                        data: {
                            key: 'file_id',
                            value: file_id
                        }
                    });
                }
            });
        }
        request('https://www.googleapis.com/drive/v2/files/' + appfolder_id, {
            loaded: function(res) {
                appfolder_id = res.id;
                localStorage.setItem('appfolder_id', res.id);
                if ((res.properties) && (res.properties.file_id)) {
                    file_id = res.properties.file_id;
                    localStorage.setItem('file_id', file_id);
                } else {
                    make_file();
                }
            }
        });
    } else {
        request('https://www.googleapis.com/drive/v2/files/' + file_id + '?alt=media', {
            loaded: function(res){
                console.log(res);
            }
        });
    }
})();


