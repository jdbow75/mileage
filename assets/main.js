angular.module('mileageApp', ['ui.bootstrap', 'ui.router']).
config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $stateProvider.
        state('main', {
            templateUrl: 'pages/main.html',
            controller: ['$scope', '$state', 'trip_data', function($scope, $state, trip_data) {
                $scope.isNavCollapsed=true;
                $scope.$on('$stateChangeStart', function () {
                    $scope.isOffcanvasActive=false;
                });
                $scope.list = trip_data.list;
                $scope.selectMonth = function (month) {
                    $state.go('main.list', {'month': month});
                };
            }]
        }).
        state('main.new', {
            url: '/',
            templateUrl: 'pages/edit.html',
            controller: 'EditController'
        }).
        state('main.edit', {
            url: '/{id:[0-9]+}',
            templateUrl: 'pages/edit.html',
            controller: 'EditController'
        }).
        state('main.list', {
            url: '/list/{month:20\\d\\d-(?:0[1-9]|1[012])}',
            templateUrl: 'pages/list.html',
            controller: function($filter, $scope, $stateParams, trip_data) {
                $scope.month = $stateParams.month;
                $scope.list = $filter('filter')(trip_data.list, {date: $stateParams.month});
            }
        }).
       state('config', {
            url: '/config',
            template: "What could you possibly want to configure?"
        }).
       state('about', {
            url: '/about',
            template: "This app is lovingly built and maintained by Jonathan \
                       Bowman."
        }); 
}]).
controller('EditController', ['$filter', '$scope', '$state', '$stateParams', 'trip_data', function($filter, $scope, $state, $stateParams, trip_data) {
    if ($stateParams.id) {
        var trip = angular.copy(trip_data.get($stateParams.id));
        if (trip) {
            /*
            var d = trip.date.split('-').map(function(item) {return parseInt(item);});
            trip.date = new Date(d[0],d[1]-1,d[2]);
            */
            trip.date = $filter('parseDate')(trip.date);
            $scope.trip = trip;
        } else {
            $state.go('main.new');
        }
    } else {
        $scope.trip = {date: new Date()};
    }
    $scope.save = function () {
        trip_data.save($scope.trip);
        $state.go('main.edit',{'id': $scope.trip.id});
    };
}]).
factory('trip_data', ['$filter', '$window', function ($filter, $window) {
    var trips = angular.fromJson($window.localStorage.getItem('trips')) || {list:[], lookup: []};
    var next_id = trips.list.length;
    if (next_id) {
        next_id = trips.list[next_id - 1].id + 1;
    }
    return {
        get: function (id) {
            return trips.list[trips.lookup[id]];
        },
        list: trips.list,
        save: function (record) {
            var trip = angular.copy(record);
            trip.date = $filter('date')(trip.date, 'yyyy-MM-dd');
            if (isNaN(trip.id)) {
                record.id = trip.id = next_id;
                trips.lookup[next_id] = trips.list.push(trip) - 1;
            } else {
                var id = trip.id;
                trips.list[trips.lookup[id]] = trip;
            }
            $window.localStorage.setItem('trips', angular.toJson(trips));
        }
    };
}]).
filter('filterMonths', function() {
    return function(input) {
        var months = [];
        for (var i=0; i < input.length; i++) {
            var month = input[i].date.slice(0,7);
            if (months.indexOf(month)) {
                months.push(month);
            }
        }
        return months.sort();
    };
}).
filter('getMonth', function() {
    return function(input) {
        return input.slice(0,7);
    };
}).
filter('parseDate', function() {
    return function(input) {
        return new Date(input.replace(/-/g, ','));
    };
});
