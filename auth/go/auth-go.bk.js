var authgo = authgo || {};

authgo.b64urlEncode = function(input) {
    return btoa(input).replace(/\+/g, '-').replace(/\//g, '_')
};

authgo.init = function () {
    var _ = authgo;
    if (_.params && _.bytes) {
        _.params.state = _.b64urlEncode(String.fromCharCode.apply(null, _.bytes));
        localStorage.setItem('auth.nonce', _.params.state);
        if (!location.host.indexOf('localhost') || !location.host.indexOf('192.168')) {
            _.params.state += '.' + _.b64urlEncode(location.host);
        }
        _.params.response_type = 'token';
        var auth_id = localStorage.getItem('auth.id');
        if (auth_id) {
            _.params.login_hint = auth_id;
        }

        var parts = [];
        for(var p in _.params) {
            if (_.params.hasOwnProperty(p)) {
                parts.push(encodeURIComponent(p) + "=" + encodeURIComponent(_.params[p]));
            }
        }
        _.uri += '?' + parts.join("&");

        window.location.href = _.uri;

    }
};

(function (){
    var _ = authgo;

    localStorage.setItem('auth.return', document.referrer);

    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        authgo.params = JSON.parse(xhr.responseText);
        authgo.uri = authgo.params.auth_uri;
        delete authgo.params.auth_uri;
        delete authgo.params.token_uri;
        authgo.init();
    };
    xhr.open("GET", '../config.json');
    xhr.send();

    _.NONCE_SIZE = 24;

    var _crypto = window.crypto || window.msCrypto;
    if(_crypto && _crypto.getRandomValues) {
        _.bytes = new Uint8Array(_.NONCE_SIZE);
        _crypto.getRandomValues(_.bytes);    
        authgo.init();
    } else {
        var s = document.createElement("script");
        s.onload = function () {
            authgo.bytes = [];
            for (var i = 0; i < authgo.NONCE_SIZE; i++) {
                authgo.bytes.push(Math.floor(isaac.random()*256));
            }
            authgo.init();
        };
        s.src = 'isaac.min.js';
        document.head.appendChild(s);
    }
    
})();
