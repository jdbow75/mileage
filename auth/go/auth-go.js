(function (){
    var NONCE_SIZE = 24;
    var bytes, params, uri;

    function b64urlEncode (input) {
        return btoa(input).replace(/\+/g, '-').replace(/\//g, '_')
    }

    function init () {
        if (params && bytes) {
            params.state = b64urlEncode(String.fromCharCode.apply(null, bytes));
            localStorage.setItem('auth.nonce', params.state);
            if (!location.host.indexOf('localhost') || !location.host.indexOf('192.168')) {
                params.state += '.' + b64urlEncode(location.host);
            }
            params.response_type = 'token';
            var auth_id = localStorage.getItem('auth.id');
            if (auth_id) {
                params.login_hint = auth_id;
            }

            var parts = [];
            for(var p in params) {
                if (params.hasOwnProperty(p)) {
                    parts.push(encodeURIComponent(p) + "=" + encodeURIComponent(params[p]));
                }
            }
            uri += '?' + parts.join("&");

            location.href = uri;

        }
    }

    localStorage.setItem('auth.return', document.referrer);

    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        params = JSON.parse(xhr.responseText);
        uri = params.auth_uri;
        delete params.auth_uri;
        delete params.token_uri;
        init();
    };
    xhr.open("GET", '../config.json');
    xhr.send();


    var _crypto = window.crypto || window.msCrypto;
    if(_crypto && _crypto.getRandomValues) {
        bytes = new Uint8Array(NONCE_SIZE);
        _crypto.getRandomValues(bytes);    
        init();
    } else {
        var s = document.createElement("script");
        s.onload = function () {
            bytes = [];
            for (var i = 0; i < NONCE_SIZE; i++) {
                bytes.push(Math.floor(isaac.random()*256));
            }
            init();
        };
        s.src = 'isaac.min.js';
        document.head.appendChild(s);
    }
    
})();
