(function (){

var params = {};
var queryString = location.hash.substring(1);
var regex = /([^&=]+)=([^&]*)/g;
var m;
while (m = regex.exec(queryString)) {
    params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
}
if (params.state) {
    var state = params.state.split('.');
    if (state.length > 1) {
        var host = atob(state[1].replace(/-/g, '+').replace(/_/g, '/'))
        window.location = 'http://' + host + '/auth/' + location.hash.replace('.' + encodeURIComponent(state[1]),'');
    }

    var received_nonce = state[0];
    if (received_nonce === localStorage.getItem('auth.nonce') && params.access_token) {
        localStorage.removeItem('auth.nonce');
        var xhr = new XMLHttpRequest();
        xhr.open("GET", 'config.json', false);
        xhr.send();
        var config = JSON.parse(xhr.responseText);

        xhr.open("POST", config.token_uri, false);
        xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhr.send('access_token=' + params.access_token);
        var res = JSON.parse(xhr.responseText);
        if (res.audience === config.client_id) {
            localStorage.setItem('auth.token', params.access_token);
            localStorage.setItem('auth.id', res.user_id);
            localStorage.setItem('auth.expires', new Date().getTime() + (res.expires_in-2)*1000);
            var dest = localStorage.getItem('auth.return');
            if (dest) {
                window.close();
                window.location.href = dest;
            } else {
                window.location.href = window.location.href.slice(0,location.href.indexOf('/auth')+1);
            }
        }
    }

}


})();

